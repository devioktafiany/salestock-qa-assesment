Feature:	Login Feature

	Scenario: Valid_credential_login
		Then I navigate to "http://automationpractice.com/index.php"
		Then I wait for 10 sec
		Then I click on element having xpath "//div/div[1]/header/div[2]/div/div/nav/div[1]/a"
		Then I enter "devi0@gmail.com" into input field having id "email_create"
		Then I click on element having id "SubmitCreate"
		Then I wait for 10 sec
		Then I enter "Devi2" into input field having id "customer_firstname"
		Then I enter "aaaa" into input field having id "customer_lastname"
		Then I enter "12345" into input field having id "passwd"
		Then I enter "12345" into input field having id "address1"
		Then I select "id_state" option by text from dropdown having id "Indonesia"
		Then I enter "Jakarta" into input field having id "city"
		Then I enter "12345" into input field having id "postcode"
		Then I enter "09876542" into input field having id "phone-mobile"
		Then I click on element having id "submitAccount"