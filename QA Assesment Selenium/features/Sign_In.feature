Feature:	Login Feature

	Scenario: Valid_credential_login
		Then I navigate to "http://automationpractice.com/index.php"
		Then I wait for 20 sec
		Then I click on element having xpath "//div/div[1]/header/div[2]/div/div/nav/div[1]/a"
		Then I enter "devi@gmail.com" into input field having id "email"
		Then I enter "password" into input field having id "passwd"
		Then I click on element having id "SubmitLogin"
		Then I click on element having xpath "//div/div[1]/header/div[2]/div/div/nav/div[2]/a"